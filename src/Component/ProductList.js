import React, { Component } from 'react'
import { Table} from 'react-bootstrap'

export default function ProductList() {
    
        return (
            <div>
                <Table responsive>
                        <thead>
                            <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Import Date</th>
                            <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <td>1</td>
                            <td>Carabao</td>
                            <td>09-03-2020</td>
                            <td> <button>Hide</button> </td>
                            </tr>
                            <tr>
                            <td>2</td>
                            <td>Pepsi</td>
                            <td>10-01-2020l</td>
                            <td><button>Hide</button></td>
                            </tr>
                            <tr>
                            <td>3</td>
                            <td>Samurai</td>
                            <td>12-03-2020l</td>
                            <td><button>Hide</button></td>
                            </tr>
                            <tr>
                            <td>4</td>
                            <td>Coca Cola</td>
                            <td>12-04-2020</td>
                            <td><button>Hide</button></td>
                            </tr>
                            <tr>
                            <td>5</td>
                            <td>Bacchus</td>
                            <td>12-04-2019</td>
                            <td><button>Hide</button></td>
                            </tr>
                        </tbody>
                        </Table>
            </div>
        )
    }

